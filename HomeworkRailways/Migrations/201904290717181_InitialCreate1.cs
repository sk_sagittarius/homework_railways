namespace HomeworkRailways.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "TrainNumber", c => c.String());
            AddColumn("dbo.Tickets", "CarriageNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Tickets", "CoupeNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Tickets", "SeatNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "SeatNumber");
            DropColumn("dbo.Tickets", "CoupeNumber");
            DropColumn("dbo.Tickets", "CarriageNumber");
            DropColumn("dbo.Tickets", "TrainNumber");
        }
    }
}
