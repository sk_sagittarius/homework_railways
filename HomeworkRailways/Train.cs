﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkRailways
{
    public class Train : Entity
    {
        // ненужный класс оказался, ничего из этого не используется, не удаляю, потому что писала несколько дней
        // с перерывами на работе и вдруг где-то что-то да упадет
        public string TrainNumber { get; set; }
        public string TrainDirection { get; set; }
        public int[] TrainCarriage { get; set; } = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; // в вагоне только десять вагонов от 1 до 10
    }
}
