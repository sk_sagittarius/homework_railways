namespace HomeworkRailways.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addboolpayed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Payed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "Payed");
        }
    }
}
