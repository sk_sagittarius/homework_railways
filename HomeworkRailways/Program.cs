﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkRailways
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowAll showAll = new ShowAll();
            Ticket ticket = new Ticket();
            //showCoupe.Show(); // просто показ купе

            #region Выбор нового билета и добавление данных
            try
            {
                Console.WriteLine("Choose carriage");
                int chosenCarriage = int.Parse(Console.ReadLine());
                showAll.ShowCoupe();
                Console.WriteLine("\nChoose free seat");

                int chosenSeat = int.Parse(Console.ReadLine());
                showAll.Show(chosenCarriage, chosenSeat);
            }
            catch(FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            #endregion

            // показ всех билетов, выбор по имени. предполагается что это все с одной учетки
            // сначала показывает Id и имя, выбирается Id и показывает всю строку
            //showAll.ShowTicket(); 

            Console.Read();
        }
    }
}
